#!/bin/bash

NUM_IMAGES="${1:-2}"

if [ "$NUM_IMAGES" -eq "$NUM_IMAGES" ] 2>/dev/null; then
    echo "Retaining ${NUM_IMAGES} most recent builds"
else
  echo "${NUM_IMAGES} is not a number"
  exit 1
fi


ALL_TAGS=$(skopeo inspect --creds="${SKOPEO_REGCRED}" "docker://${OUTPUT_IMAGE}" | jq -r '.["RepoTags"] | .[]' | grep -- "${IMAGE_PATTERN}" | sort -n)

echo "Found matching tags for this branch:"
echo "${ALL_TAGS}"
OLD_TAGS=$(echo "${ALL_TAGS}" | head -n "-${NUM_IMAGES}")

for tag in $OLD_TAGS; do
    echo "Removing tag: $tag"
    skopeo delete --creds="${SKOPEO_REGCRED}" "docker://$CI_REGISTRY/$CI_PROJECT_PATH:${tag}"
done
