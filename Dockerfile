FROM archlinux/base
# I'd love to build the static go binary but it's a whole kettle of worms, I'm going
# to be stuck with a fatter glibc distro like arch or debian until I can figure it out
RUN pacman -Syu --noconfirm && rm -fr /var/cache/pacman/pkg/*
RUN echo 'LANG=en_US.UTF-8' > /etc/locale.conf
RUN echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
RUN ln -s core_perl/pod2man /usr/bin/pod2man
RUN pacman -S --noconfirm git make autoconf binutils fakeroot sudo jq gettext unzip docker \
    buildah skopeo podman &&\
    rm -fr /var/cache/pacman/pkg/*
ENV BUILDDIR /tmp/pikaur
ENV PKGDEST /tmp/pikaur
ENV SRCDEST /tmp/pikaur
RUN useradd --no-create-home --shell=/bin/false build && usermod -L build
RUN cd /tmp/ && git clone https://aur.archlinux.org/pikaur.git && cd /tmp/pikaur &&\
    source /tmp/pikaur/PKGBUILD &&\
    pacman -S --noconfirm --needed --asdeps "${makedepends[@]}" "${depends[@]}" &&\
    cd /tmp/pikaur && chown -R build /tmp/pikaur &&\
    sudo -u build -E sh -c 'cd /tmp/pikaur && makepkg' &&\
    pacman -U --noconfirm /tmp/pikaur/*.pkg.tar.xz && cd / && rm -fr /tmp/pikaur &&\
    rm -fr /var/cache/pacman/pkg/*
RUN mkdir -p /home/build
RUN chown build /home/build
RUN echo 'build ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN chmod 400 /etc/sudoers
USER build
RUN pikaur -S --noconfirm kubernetes-helm-bin openshift-source-to-image &&\
    sudo rm -fr /var/cache/pacman/pkg/* && rm -fr ~/.cache
USER root
RUN mkdir /tmp/okd
RUN curl -L https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz > /tmp/okd.tar.gz &&\
    tar -xzf /tmp/okd.tar.gz -C /tmp/okd &&\
    mv /tmp/okd/*/oc /bin/oc &&\
    mv /tmp/okd/*/kubectl /bin/kubectl &&\
    rm -fr /tmp/okd*
ADD remove-old-images.sh /usr/bin/
