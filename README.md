[![pipeline status](https://gitlab.oit.duke.edu/devil-ops/gitlab-openshift-helper/badges/master/pipeline.svg)](https://gitlab.oit.duke.edu/devil-ops/gitlab-openshift-helper/commits/master)
[![coverage report](https://gitlab.oit.duke.edu/devil-ops/gitlab-openshift-helper/badges/master/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/gitlab-openshift-helper/commits/master)

# Gitlab Openshift Helper

A container to be used with a gitlab ci kubernetes runner, which provides some Openshift-specific helpers.

This container exists to help deal with k8s/okd operations when run via gitlab runner using the "kubernetes" executor. There are some gotchas with doing this, so I figured I would document them here.


## Setting up gitlab CI itself

Your gitlab runner needs something close to an 'admin' type RoleBinding in a namespace (in OKD, namespaces are referred to as "projects") in order to perform its various tasks.

You can choose to run gitlab runner in the same namespace as your production application. Be aware of the security implications of this configuration. Although it is possible to configure the runner in a fairly restricted way, you might wish to simply run it in a dedicated CI namespace to avoid the possibility of a misconfiguration impacting production and to make cleanup straightforward.

## Yaml time

You'll need at a minimum a *ServiceAccount* (used by the runner to manipulate pods etc), a *RoleBinding* (which associates a Role with that ServiceAccount so it has the correct permissions), a *PersistentVolumeClaim* (to hold the gitlab runner configuration), and a *Deployment* (which will run the runner's pod itself).

You will apply these with kubectl/oc apply -f <yaml>.

**TLDR**: oc apply -f \*.yaml

All yaml files are documented inline below:

PersistentVolumeClaim (named gitlab-conf):

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: gitlab-conf
spec:
  accessModes:
  - ReadWriteOnce
  storageClassName: glusterfs-storage
  resources:
    requests:
      storage: 5Gi
```

ServiceAccount (named gitlab-ci):

**Note: this is by default only accessible to the runner container itself, not any spawned pods as part of the CI process; the pods *spawned* by the runner will only have default privileges**
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-ci
```

RoleBinding (named gitlab-ci-rolebinding)

```yaml
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: gitlab-ci-rolebinding
subjects:
  - kind: ServiceAccount
    name: gitlab-ci
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: admin
```

Finally, the Deployment (name: gitlab-deployment). Note that it mounts the volume we just created at /.gitlab-runner (where the config.toml will be placed)

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: gitlab-deployment
  labels:
    app: gitlab
spec:
  replicas: 1
  selector:
    matchLabels:
      app: gitlab
  template:
    metadata:
      labels:
        app: gitlab
    spec:
      serviceAccountName: gitlab-ci
      containers:
      - name: gitlab
        image: gitlab/gitlab-runner
        volumeMounts:
        - mountPath: "/.gitlab-runner"
          name: gitlab-conf
      volumes:
        - name: gitlab-conf
          persistentVolumeClaim:
            claimName: gitlab-conf
```

Your runner should now be running in a pod, congrats!

## Register the runner

Runners can be registered as either 1) "Shared" runners (anybody can use them) or 2) "Specific" runners (tied to a project or group). Shared runners have additional security implications, and registration on a per-project level can be cumbersome, so the easiest/best compromise is to use a "Specific" runner that is registered for a "Group" in gitlab unless you know otherwise.

First, get your token from your Group's space in the web UI: Groups -> Your Groups -> (find the group in the list) -> Settings -> CI/CD -> Runners (*Expand*)

You will see the URL and token required to register the runner. You need to use that when shelling into your pod to configure the runner.

Assuming you used the same name I did for your gitlab deployment, this will reliably get a shell into the container:

```
kubectl exec -ti `kubectl get pods | grep gitlab-deployment | awk '{ print $1 }'` bash
```

To register the runner, you should be able to run:

```
kubectl exec `kubectl get pods | grep gitlab-deployment | awk '{ print $1 }'` -- gitlab-runner register --non-interactive --executor kubernetes --url https://gitlab.oit.duke.edu  --tag-list YOURTAG --kubernetes-service-account gitlab-ci --env "HOME=/tmp" --kubernetes-namespace=YOURNAMESPACE --registration-token YOURTOKEN
```

Congrats, your runner is ready to be used! In the example above, tag-list is optional, and it's a way to configure your projects to only run on runners with a tag you specify. 

# Making your projects ready to build in this kind of runner

The runner itself is good to go! Most of the normal gitlab [kubernetes executor](https://docs.gitlab.com/runner/executors/kubernetes.html) features will work. You can launch containers by specifying image: stanzas and connect to them using localhost:<port> within other containers spawned during the same job. When the job is over all these containers are destroyed automatically.

There are however some really important caveats, mostly due to the security restrictions in our Cluster. This project has a .gitlab-ci.yml included and comments on some of these limitations, but they're also described below.

## Docker registry

This project uses the gitlab docker registry. You don't have to use that, but it's super convenient if you want to use it. Set it up under Settings -> General -> Permissions -> Docker Registry

If you do set it up, you get an ephemeral token to access the registry for each build via the runner. Nice!

## You can't run docker directly

So yeah, this is the big one - you can't be privileged here and you can't even be UID 0, so you can't even run docker to do basic things with it. The only way you can be root in our OKD is within the confines of a pod launched via a BuildConfig - an OKD specific concept which is used to build Docker style images. Using a BuildConfig you can effectively do "docker build" type operations, which are otherwise impossible for us.

This is essentially the purpose of the Dockerfile in this very repo - it adds some binaries you can use to do things you'd normally do with a docker command and shows how they can be used.  Check out the build section in this yaml for a way to build containers in gitlab ci using BuildConfigs and the tag section to see how you can use skopeo to write tags.

## Our cluster doesn't support chained build containers (the docker COPY --from=0 stuff)

This sucks but our OKD seems incapable of utilizing intermediary containers. This may be a version specific limitation and we may get this feature later. Or maybe not. But until then, you need to think about the space used by your layers when you do your build, or chain BuildConfigs together somehow, or pass gitlab artifacts around to move them between images. Or you know just use alpine and don't feel too bad about big images...

The RHAT specific workaround is using a docker build approach known as source2image which injects your code into a prebuilt binary structure from a "builder" image. You can go that route if you like to keep image size down, but the prebaked source2image builders are based on Centos and use RHAT software collections, which I really don't like. For an app that has no extra dependencies, though, they can get the job done; to use them your buildconfig would use the "source2image" strategy instead of "docker" (in the 'oc new-build' command).

I think the expressiveness and simplicity of the single Dockerfile wins for me, but YMMV.

## Your containers all need to be able to run as normal users, too

No root means no root! You can't be root in a container, you can't modify stuff in the filesystem unless you specifically allow for it. In our cluster your only guarantee about the user you get is that *they will have gid 0*, which means at least that they can modify anything which is set to **group: root** with **group write** in the container. So if you need to write anything, make sure your docker image is setting chown :root && chmod g+w to that location first.

# Deploying to okd/k8s from a runner

Possibly the simplest approach is to configure an ImageStream to watch your gitlab docker registry for a well-known tag, use DeploymentConfigs that reference that ImageStream and auto-redeploy on a change. In this configuration your deploy phase is effectively your tag stage, and the primary benefit is you don't need your runner to have any OKD specific information to deploy. The downside is that your ImageStream will need a k8s ServiceAccount that can access your project's registry if that registry is private. If you need such a thing, use the following command with your gitlab registry deploy token named 'regcred':

**NB: you may need to specify the port with your registry server, e.g. gitlab.oit.duke.edu:443**
```
kubectl create secret docker-registry regcred --docker-registry=$GITLAB_REGISTRY_SERVER --docker-username=$DEPLOY_TOKEN --docker-password=$DEPLOY_TOKEN_PASSWORD
```

To create an ImageStream that can pull from your gitlab registry, use yaml like the following (*again, note the :443*):

```yaml
apiVersion: image.openshift.io/v1
kind: ImageStream
metadata:
  labels:
    app: perfdump
  name: imagestream-gitlab
spec:
  lookupPolicy:
    local: false
  tags:
  - from:
      kind: DockerImage
      name: gitlab-registry.oit.duke.edu:443/devil-ops/gitlab-openshift-helper:latest
    name: latest
    importPolicy:
      scheduled: true
status:
  dockerImageRepository: ''
```

Alternately, you can also use the cli to create an imagestream simply, like so:

```shell
oc import-image gitlab-registry.oit.duke.edu:443/devil-ops/gitlab-openshift-helper:latest --confirm
```

The only real downside I can find with this is that you can't seem to specify a secret to use in any particular ImageStream - rather, I think it matches against the docker-registry URI in the credential definition. In most cases this should be fine, but it might not work if you deploy from multiple private registries on the same gitlab server without a gitlab "Personal Access Token" (which can pull registries from all the projects the user can pull, but is associated with a User in gitlab instead of a Project).

## Other deployment approaches

When I was using Deployments, I would deploy directly out of gitlab's docker registry, and modify each Deployment to reference the new docker image by a unique tag that is generated at build time. This is pretty straightforward to do, but you need to create a Role and corresponding RoleBinding with the ability to perform such a modification in your production namespace, and you *also* need to modify every single resource that you intend to deploy to, so if you have multiple Deployments or DeploymentConfigs like this you need to touch them all.

The final option I pondered (but never tried) was using Skopeo to copy the final image into the OKD docker registry which is backed by an ImageStream. This option only really makes sense when using DeploymentConfigs, which can reference ImageStreams and be made to rebuild when ImageStreams change. This has the downside that you can't readily use the default k8s environment variables for your credentials - even though you can give the gitlab service account RoleBindings to modify the ImageStream(s) you care about, skopeo does not understand how to use the k8s api to get docker creds from a k8s service account, so it has no built-in way to get the docker token type Secret you need to use. You'd need to either define the ImageStream credentials in gitlab ci and reference those in your YAML (always an option, but the more configuration points like that the more fiddly things get) or write some kind of helper to look them up via the k8s api (which starts to clutter up the yaml file even more - the complexity is already on the high end of what I consider tolerable).
